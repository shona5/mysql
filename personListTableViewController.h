//
//  personListTableViewController.h
//  contactModuleCrmTable
//
//  Created by Don Asok on 16/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "personAddViewController.h"


@interface personListTableViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource,PersonAddDelegate>


@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
