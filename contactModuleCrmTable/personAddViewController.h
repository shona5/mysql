//
//  personAddViewController.h
//  contactModuleCrmTable
//
//  Created by Don Asok on 16/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PersonAddDelegate;
@class Demo;

@interface personAddViewController : UIViewController
@property (nonatomic, strong) Demo *demo1;
@property (nonatomic, unsafe_unretained) id <PersonAddDelegate> delegate;

@end
@protocol PersonAddDelegate <NSObject>

- (void)PersonController:(personAddViewController *)PersonController didAddPerson:(Demo *)personF;

@end
