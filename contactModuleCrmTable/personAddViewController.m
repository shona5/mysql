//
//  personAddViewController.m
//  contactModuleCrmTable
//
//  Created by Don Asok on 16/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//

#import "personAddViewController.h"
#import "Demo.h"

@interface personAddViewController () <UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField *nameTextField;

@end

@implementation personAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Add Person";
    
    [self.nameTextField becomeFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.nameTextField) {
        [self.nameTextField resignFirstResponder];
        [self save:self];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
- (IBAction)save:(id)sender
{
    self.demo1.firstName = self.nameTextField.text;
    
    NSError *error = nil;
    if (![self.demo1.managedObjectContext save:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    
     [self.delegate PersonController:self didAddPerson:self.demo1];
}
- (IBAction)cancle:(id)sender
{
    [self.demo1.managedObjectContext deleteObject:self.demo1];
    
    NSError *error = nil;
    if (![self.demo1.managedObjectContext save:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    [self.delegate PersonController:self didAddPerson:nil];
}

    



@end
