//
//  Demo+CoreDataProperties.h
//  contactModuleCrmTable
//
//  Created by Don Asok on 16/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Demo.h"

NS_ASSUME_NONNULL_BEGIN

@interface Demo (CoreDataProperties)

@property (nullable, nonatomic, retain) id image;
@property (nullable, nonatomic, retain) NSString *firstName;
@property (nullable, nonatomic, retain) NSString *lastName;
@property (nullable, nonatomic, retain) NSNumber *phone;

@end

NS_ASSUME_NONNULL_END
