//
//  Demo+CoreDataProperties.m
//  contactModuleCrmTable
//
//  Created by Don Asok on 16/12/16.
//  Copyright © 2016 Don Asok. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Demo+CoreDataProperties.h"

@implementation Demo (CoreDataProperties)

@dynamic image;
@dynamic firstName;
@dynamic lastName;
@dynamic phone;

@end
